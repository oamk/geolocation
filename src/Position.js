import React from 'react';

export default class Position extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lat: 0,
      lng: 0
    }
  }

  componentDidMount() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {     
        this.setState({
          lat: position.coords.latitude,
          lng: position.coords.longitude
        });
      }, (error) => {
        alert(error);
      })
    }
    else {
      alert('Your browser does not support geolocation!');
    }
  }

  render() {
    const {lat, lng} = this.state;
    console.log(lat);
    return (
      <>
        <p>Position: {lat.toFixed(3)}, {lng.toFixed(3)}</p>
      </>
    )
  }
}









